package mk.iwec.FurnitureSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FurnitureSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(FurnitureSpringBootApplication.class, args);
	}

}
