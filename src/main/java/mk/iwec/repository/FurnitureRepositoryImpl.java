package mk.iwec.repository;

import java.util.Map;

import org.springframework.stereotype.Repository;

import mk.iwec.model.Chair;
import mk.iwec.model.Cupboard;
import mk.iwec.model.Teatable;
import mk.iwec.service.FurnitureCalculation;

@Repository

public class FurnitureRepositoryImpl implements FurnitureRepository {
	private Map <Integer,Cupboard> cb;
	private Map <Integer,Chair> ch;
	private Map <Integer,Teatable> tt;
	
	public Cupboard recognition(Integer wood) {
		boolean check = cb.containsKey(wood);
		if (!check) {
			throw new IllegalStateException("its not cupboard");
		}
		return cb.get(wood);
	}

	
	public Chair recognition(Integer wood, Integer glass) {
		boolean check = ch.containsKey(wood) & ch.containsKey(glass);
		if (!check) {
			throw new IllegalStateException("its not chair");
		}
		return ch.get(glass);
	}

	public Teatable recognition(Integer wood, Integer glass, Integer plastic) {
		boolean check = tt.containsKey(wood) & tt.containsKey(glass) & tt.containsKey(plastic);
		if (!check) {
			throw new IllegalStateException("its not teatable");
		}
		return null;
	}

	
	public FurnitureCalculation compare(Integer wood, Integer glass, Integer plastic, Integer number) {
		if (wood*number > (wood+glass)*number & wood*number > (wood+plastic+glass)*number ) {
			System.out.println("its cupboard");
		}
		if ((wood+glass)*number > wood*number & (wood+glass)*number > (wood+plastic+glass)*number ) {
			System.out.println("its chair");
		}
		if ( (wood+plastic+glass)*number > wood*number &  (wood+plastic+glass)*number > (wood+plastic)*number ) {
			System.out.println("its teatable");
		}
		
		return null;
	}

}
