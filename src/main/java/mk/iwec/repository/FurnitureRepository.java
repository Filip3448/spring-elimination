package mk.iwec.repository;

import mk.iwec.model.Chair;
import mk.iwec.model.Cupboard;
import mk.iwec.model.Teatable;
import mk.iwec.service.FurnitureCalculation;

public interface FurnitureRepository {
	public Cupboard recognition(Integer wood);
	public Chair recognition(Integer wood,Integer glass);
	public Teatable recognition (Integer wood,Integer glass,Integer plastic);
	public FurnitureCalculation compare(Integer wood,Integer glass, Integer plastic, Integer number);
	
}
