package mk.iwec.service;

import org.springframework.stereotype.Service;

@Service
public class FurnitureCalculationImpl implements FurnitureCalculation {

	@Override
	public int cupboardCalculation(int wood, int number) {
		
		return wood * number;
	}

	@Override
	public int chairCalculation(int wood, int glass, int number) {
		
		return (wood + glass)* number;
	}

	@Override
	public int teatableCalculation(int wood, int glass, int plastic, int number) {
		
		return (wood+glass+plastic)*number;
	}

}
