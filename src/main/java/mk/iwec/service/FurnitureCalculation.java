package mk.iwec.service;

public interface FurnitureCalculation {
	
	int cupboardCalculation(int wood,int number);
	int chairCalculation(int wood,int glass,int number);
	int teatableCalculation(int wood,int glass,int plastic,int number);
}
