package mk.iwec.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Teatable {
	private int wood;
	private int glass;
	private int plastic;
	private int number;
}
