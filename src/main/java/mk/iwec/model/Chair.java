package mk.iwec.model;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Chair {
	private int wood;
	private int glass;
	private int number;
}
