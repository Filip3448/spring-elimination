package mk.iwec.model;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Cupboard {
	private int wood;
	private int number;
}
