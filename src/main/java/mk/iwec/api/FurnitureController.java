package mk.iwec.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mk.iwec.model.Chair;
import mk.iwec.model.Cupboard;
import mk.iwec.model.Teatable;
import mk.iwec.service.FurnitureCalculation;

@RestController
@RequestMapping(path = "/furniture")
public class FurnitureController {

	@Autowired
	private FurnitureCalculation furniturecalc;
	
	@GetMapping
	Integer operation(@RequestParam Integer wood, @RequestParam Integer glass, @RequestParam Integer plastic,@RequestParam Integer number,@RequestParam Character o) {
		switch (o) {
		case 'C':
			return furniturecalc.cupboardCalculation(wood, number);
		case 'H':
			return furniturecalc.chairCalculation(wood, glass, number);
		case 'T':
			return furniturecalc.teatableCalculation(wood, glass, plastic, number);
		default:
			return null;
		}
	}
	
	@GetMapping
	public Cupboard recognition(@PathVariable ("wood") Integer Wood, @PathVariable ("number") Integer number) {
		furniturecalc.cupboardCalculation(Wood, number);
		return null;
	}
	@GetMapping
	public Chair recognition(@PathVariable ("wood") Integer Wood,@PathVariable ("glass") Integer glass,@PathVariable ("number") Integer number) {
		furniturecalc.chairCalculation(Wood, glass, number);
		return null;
	}
	@GetMapping
	public Teatable recognition(@PathVariable ("wood") Integer wood, @PathVariable ("glass") Integer glass,@PathVariable ("plastic") Integer plastic,@PathVariable ("number") Integer number) {
		furniturecalc.teatableCalculation(wood, glass, plastic, number);
		return null;
	}
	
	
}
